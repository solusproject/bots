#! /bin/sh

echo ===============================
echo ==== Updating Dependencies ====
echo ===============================
echo Updating Bot Dependencies
npm install discord.js@latest

echo Updating AI Dependencies
npm install @nlpjs/basic@latest
npm install @nlpjs/nlp@latest
npm install @nlpjs/emoji@latest
npm install @nlpjs/request@latest
npm install node-stopwords-filter@latest

echo Updating Database Depependencies
npm install @supabase/supabase-js@latest

echo =======================
echo ==== Running Audit ====
echo =======================
npm audit
npm audit fix --force

echo Starting Quinn
cd ../
node src/index.js
