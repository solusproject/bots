/*
Quinn is designed with the intention to help,
but is provided with no warranty of any kind, implied
or otherwise.

Solus is protected under the MIT License

(c) MIT License by the Solus Foundation

We have compressed Quinn into multiple different
files, one of which is core.js
which is designed to make updates to Sophie
as universal and painless as possible

index.js is used to load the needed files and keep
them loaded should a crash start, to minimize downtime

any files in /bots are to be loaded below

training the AI here so it doesn't have to be done
on every crash to minimize downtime
*/

async function init() {
  /* Clear the console and announce the AI is loading */
  console.clear();
  console.log("Loading Quinn AI")

  /* Load the AI libraries and configure it */
  const { Nlp } = require('@nlpjs/nlp');
  const { fs } = require('@nlpjs/request');
  const { removeEmojis } = require('@nlpjs/emoji');
  const nlp = new Nlp({ languages: ['en'], threshold: 0.8 });
  nlp.container.register('fs', fs);

  /* @TODO: Reduce dependencies */

  /* Configure Stopwords */
  var StopwordsFilter = require('node-stopwords-filter');
  var f = new StopwordsFilter();

  /* Load the model and language */
  const model = require("./model.json")
  nlp.addLanguage('en');

  /* Process the model */
  for (const key of Object.keys(model)) {
    for (let string of model[key]) {
      string = removeEmojis(f.filter(string).join(" "))
      nlp.addDocument("en", string, key)
    }
  }

  /* Train the model */
  await nlp.train()
  console.log("Done loading AI...")

  /* Load the bots */
  console.log("Loading Quinn Bots...")

  /* Get the path */
  var botPath = require("path").join(__dirname, "bots");

  /* Run the init function of every file in the bots directory */
  require("fs").readdirSync(botPath).forEach(function (file) {
    console.log("Loading " + file)
    bot = require("./bots/" + file);
    console.log("Starting " + file)
    bot.init(nlp);
  });

  console.log("All Quinn Bots started!")
}

init()