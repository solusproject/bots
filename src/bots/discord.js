const Discord = require("discord.js");
const fs = require('fs');
const creds = require("../creds.json");
var StopwordsFilter = require('node-stopwords-filter');
const { removeEmojis } = require('@nlpjs/emoji');
var f = new StopwordsFilter();

exports.init = (classifier) => {
    console.log("Quinn is starting on Discord");
    const bot = new Discord.Client({ intents: [Discord.Intents.FLAGS.GUILDS, Discord.Intents.FLAGS.GUILD_MESSAGES] });

    bot.on("ready", () => {
        console.log("Quinn Discord Bot has started!");
        delete creds.token;
    });

    bot.on("messageCreate", async msg => {
        if (msg.author.bot) return;

        /* Remove stopwords, mentions, emojis */
        msg.content = removeEmojis(f.filter(msg.content).join(" ")
        .replace(/\s*\<.*?\>\s*/g, ''))

        /* Classify */
        let result = await classifier.process("en", await msg.content)

        if (creds.production == false) {
            /* Record the message in the pending dataset */
            fs.appendFileSync("model.csv", msg.content + "," + result.intent + "\n")
        }

        /* Assume unknowns are 0 */
        if (result == "6") {
            result = "0"
        }

        if (creds.production == false && msg.guild.id == 928056009530949648) {
            if (msg.content != "") {
                msg.reply(`Rating: ${result.intent}\nMessage Percieved: ${msg.content}`)
            }
        }
    });

    bot.login(creds.token)
};