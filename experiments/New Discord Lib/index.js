/* Get Libs */
const { WebSocket } = require("ws")
const ws = new WebSocket('wss://gateway.discord.gg/?v=9&encoding=json', {
    perMessageDeflate: false
});
const creds = require("../../src/creds.json");

/* Get a few parameters */
const token = creds.json
const status = "invisible"

/* Config Axios */


/* Identity */
const identify = {
    "op": 2,
    "d": {
        "token": token,
        "intents": 512,
        "properties": {
            "$os": "linux",
            "$browser": "quinn",
            "$device": "quinn-server"
        }
    }
}

/* Get presence */
const presence = {
    "op": 3,
    "d": {
        "activities": [],
        "status": status,
        "afk": false
    }
}

const start = () => {

    ws.on("open", () => {
        /* Identify Self */
        ws.send(JSON.stringify(identify))
    })

    /* Set Session to global */
    let session;

    /* On message, get ID */
    ws.on('message', (data) => {
        data = JSON.parse(data.toString())
        console.log(data)

        if (data.op == 10 & data.s == null) {
            console.log(data.d.heartbeat_interval)
            setInterval(() => {
                ws.send(JSON.stringify({
                    "op": 1,
                    "d": 251
                }))
            }, data.d.heartbeat_interval - 1)
        }

        if (data.op == 0 & data.s == 2) {
            console.log('received %s by', data.d.content, data.d.author.id);
        }

        if (data.op == 0 & data.s == 1) {
            console.log("Ready!")
            session = data.session_id
            ws.send(JSON.stringify(presence))
        }
    });

    /* On connection close, attempt to reconnect */
    ws.on("close", () => {
        const resume = {
            "op": 6,
            "d": {
                "token": token,
                "session_id": session,
                "seq": 1337
            }
        }
        start(resume)
    });

    /* On error, log error */
    ws.on("error", (error) => {
        console.error(error)
    })
}

start(identify)